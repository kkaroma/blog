---
title: Second Article
date: 2020-05-28
tags: [example, expert]
---

This is an example article. This blog has been generated using [middleman](https://middlemanapp.com/basics/blogging/).

![My Photo](/blog/images/middleman.png)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

You probably want to delete it and write your own articles!
This is my first article, more to come...

**I am now an expert of the makrdown, but I am sure will get it right!**

## Sub title, h2 tag
*This is italic*
---

